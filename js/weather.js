var weatherData;
var NUM_DAYS = 14;


(function($){
    $(function(){

        var forecastList = $('#forecast-list');
        var footer = $('.page-footer');

        if (window.localStorage.getItem('id') != null) {
            loadForecasts();
        }
        //mobile and tablet
        if ($(window).width() < 993) {
            forecastList.on('click', '.collection-item', function () {
                if (animateMenuItem()) {
                    $('html, body').animate({ scrollTop: 0 }, 0);
                    $('#mobile-share-icon').removeClass('hidden');
                    var index = $('.collection-item').index($(this));
                    loadForecastItem(weatherData.list[index], index);
                    forecastList.addClass('hidden');
                    $('#forecast-item').removeClass('hide-on-med-and-down').removeClass('hidden');

                    footer.addClass('animated slideInRight');
                    footer.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() { footer.removeClass('animated slideInRight'); });

                    fixFooter();
                }
            });

            $('.button-collapse').on('click', '.in', function () {
                if (animateMenuItem()) {
                    forecastList.removeClass('hidden');
                    $('#forecast-item').addClass('hidden');
                    $('#mobile-share-icon').addClass('hidden');

                    forecastList.addClass('animated slideInLeft');
                    forecastList.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() { forecastList.removeClass('animated slideInLeft'); });
                    animateMenuItem();

                    fixFooter();
                }
            }).on('click', '.menu', function () {
                $('#menu-icon').sideNav('show');
            });
        //desktop
        } else {
            forecastList.on('click', '.collection-item', function () {
                forecastList.find('.collection-item.active').removeClass('active');
                $(this).addClass('active');
                var index = $('.collection-item').index($(this));
                loadForecastItem(weatherData.list[index], index);
            });
        }
    }); // end of document ready
})(jQuery); // end of jQuery name space

//radial action animation
function animateMenuItem() {
    var arrow = $('#arrow');
    if (!arrow.hasClass('anim')) {
        if (!arrow.hasClass('in')) {
            arrow.addClass('in')
                .removeClass('menu');
        } else {
            arrow.removeClass('in')
                .addClass('menu')
                .addClass('out')
                .delay(310)
                .queue(function (next) {
                    $('#arrow').addClass('no-trans').removeClass('out');

                    next();
                });
        }

        arrow.addClass('anim').delay(500).queue(function (next) {
            arrow.removeClass('anim');
            arrow.removeClass('no-trans');
            next()
        });
        return true;
    }
    return false;
}

function loadForecastItem(forecast, index) {
    var forecasItemElement = $('#forecast-item .toc-wrapper');
    forecasItemElement.empty();
    var forecasItem = "<div class='col l3 s12'>";
    switch (index) {
        case 0:
            forecasItem += "<h5>Hoje</h5><h6>" + formatDate(forecast.dt) + "</h6>";
            break;
        case 1:
            forecasItem += "<h5>Amanhã</h5><h6>" + formatDate(forecast.dt) + "</h6>";
            break;
        default:
            forecasItem += "<h5>" + formatDate(forecast.dt) + "</h5>";
            break;
    }
    forecasItem += "</div>";
    forecasItem += "<div class='col l9 s12'>";
    forecasItem += "<div class='left data'>";
    forecasItem += "<h1>" + formatTemp(forecast.temp.max) + ((window.localStorage.getItem('unit') == "metric") ? "°C" : "°F") + "</h1>";
    forecasItem += "<h3>" + formatTemp(forecast.temp.min) + ((window.localStorage.getItem('unit') == "metric") ? "°C" : "°F") + "</h3>";
    forecasItem += "<p>Humidade: " + forecast.humidity + "%</p>";
    forecasItem += "<p>Pressão: " + forecast.pressure + " hPa</p>";
    forecasItem += "<p>Vento: " + forecast.speed + " km/h " + formatWindDirection(forecast.deg) + "</p>";
    forecasItem += "</div>";
    forecasItem += "<figure>";
    forecasItem += "<img src='img/w/" + forecast.weather[0].icon + "-big.png'>";
    forecasItem += "<figcaption>" + formatWeather(forecast.weather[0].main) + "</figcaption>";
    forecasItem += "</figure>";
    forecasItem += "</div>";
    forecasItemElement.append($(forecasItem))
        .addClass('animated slideInRight');
    forecasItemElement.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() { forecasItemElement.removeClass('animated slideInRight'); });
}
function loadForecasts() {
    $('.mdi-social-share').parent().addClass('disabled');
    $.ajax({
        //http://api.openweathermap.org/data/2.5/forecast/daily?id=524901&units=metric&cnt=14
        url: "http://api.openweathermap.org/data/2.5/forecast/daily",
        data: { id: window.localStorage.getItem('id'), units: window.localStorage.getItem('unit'), cnt: NUM_DAYS, , appid: "3fed727e458bcfd29fd2a815777eaca2" }
    })
        .done(function( data ) {
            if (data.cod == 200) {
                $("footer").css("position", "relative");
                weatherData = data;
                for (var i = 0; i < data.list.length; i++) {
                    var forecast = data.list[i];
                    if(i == 0) {
                        loadForecastItem(forecast, 0);
                    }

                    var forecastListItem = "<a href='#!' class='collection-item waves-effect" + ((i == 0) ? " active" : "") + "'>";
                    forecastListItem +=      "<figure>";
                    forecastListItem +=        "<img src='img/w/" + forecast.weather[0].icon + ".png'>";
                    forecastListItem +=        "<figcaption>";
                    forecastListItem +=          "<div>";
                    switch (i) {
                        case 0:
                            forecastListItem +=        "<h5>Hoje" + "<h6>,&nbsp;" + formatDate(forecast.dt) + "</h6></h5>";
                            break;
                        case 1:
                            forecastListItem +=        "<h5>Amanhã" + "<h6>,&nbsp;" + formatDate(forecast.dt) + "</h6></h5>";
                            break;
                        default:
                            forecastListItem +=        "<h5>" + formatDate(forecast.dt) + "</h5>";
                            break;
                    }
                    forecastListItem +=            "<h5 class='right'>" + formatTemp(forecast.temp.max) + ((window.localStorage.getItem('unit') == "metric") ? "°C" : "°F") + "</h5>";
                    forecastListItem +=          "</div>";
                    forecastListItem +=          "<div>";
                    forecastListItem +=            "<h6>" + formatWeather(forecast.weather[0].main) + "</h6>";
                    forecastListItem +=            "<h6 class='right'>" + formatTemp(forecast.temp.min) + ((window.localStorage.getItem('unit') == "metric") ? "°C" : "°F") + "</h6>";
                    forecastListItem +=          "</div>";
                    forecastListItem +=        "</figcaption>";
                    forecastListItem +=      "</figure>";
                    forecastListItem +=    "</a>";
                    $('#forecast-list').append($(forecastListItem));
                }
                $('#forecast-list h4.collection-header').text(window.localStorage.getItem('location'));
                $('.mdi-social-share').parent().removeClass('disabled');
                $('#load').hide();
                $('[id^="forecast"]').show();
            } else {
                showError();
            }
        })
        .fail(function() {
            showError();
        })
        .always(function() {
            if ($(window).width() > 992) {
                var toc = $('.toc-wrapper');
                toc.pushpin({
                    top: toc.offset().top,
                    bottom: $('footer').offset().top
                });
            }

            if ($("html").height() < $(window).height())
                $("footer").css("width", "100%")
                    .css("position", "absolute")
                    .css("bottom", "0px");
        });
}

function showError() {
    var loadElement = $('#load');
    loadElement.empty();
    var info = '<i class="mdi-content-report large"></i>';
    info += '<h4>Erro: Não foi possível carregar as previsões deste local.</h4>';
    info += '<h5>Altere sua localização ';
    info += '<a href="config.html">';
    info += '<i class="mdi-action-settings small"></i> Clique aqui!</a>';
    info += '</h5>';
    loadElement.append($(info));
}

function formatDate(UNIX_timestamp){
    var date = new Date(UNIX_timestamp*1000);
    var months = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
    var month = months[date.getMonth()];
    date = date.getDate();
    return date + " de " + month;
}

function formatTemp(temp) {
    return Math.round(temp);
}

function formatWeather(weather) {
    switch (weather) {
        case "Thunderstorm":
            return "Tempestade";
            break;
        case "Drizzle":
            return "Neblina";
            break;
        case "Rain":
            return "Chuva";
            break;
        case "Snow":
            return "Neve";
            break;
        case "Clouds":
            return "Nuvens";
            break;
        case "Clear":
            return "Céu Aberto";
            break;
        default:
            return weather;
            break;
    }
}

//http://climate.umn.edu/snow_fence/Components/winddirectionanddegreeswithouttable3.htm
function formatWindDirection(direction) {
    if (direction > 348.75 && direction <= 11.25) {
        return "N";
    } else if (direction > 11.25 && direction <= 33.75) {
        return "NNE";
    } else if (direction > 33.75 && direction <= 56.25) {
        return "NE";
    } else if (direction > 56.25 && direction <= 78.75) {
        return "ENE";
    } else if (direction > 78.75 && direction <= 101.25) {
        return "E";
    } else if (direction > 101.25 && direction <= 123.75) {
        return "ESE";
    } else if (direction > 123.75 && direction <= 146.25) {
        return "SE";
    } else if (direction > 146.25 && direction <= 168.75) {
        return "SSE";
    } else if (direction > 168.75 && direction <= 191.25) {
        return "S";
    } else if (direction > 191.25 && direction <= 213.75) {
        return "SSW";
    } else if (direction > 213.75 && direction <= 236.25) {
        return "SW";
    } else if (direction > 236.25 && direction <= 258.75) {
        return "WSW";
    } else if (direction > 258.75 && direction <= 281.25) {
        return "W";
    } else if (direction > 281.25 && direction <= 303.75) {
        return "WNW";
    } else if (direction > 303.75 && direction <= 326.25) {
        return "NW";
    } else if (direction > 326.25 && direction <= 348.75) {
        return "NNW";
    } else {
        return ""
    }
}