(function($){
    $(function(){

        $('#menu-icon').sideNav({menuWidth: 240, activationWidth: 70});

        $('select').material_select();

        $('.tooltipped').tooltip({delay: 50});

        $(".dropdown-button").dropdown({ hover: false });

        fixFooter();

        //handle submit config form button
        $('#setting-form').submit(function() {
            submutActive(false);
            var input = $('input[name="location"]');
            if (input.val() == "") {
                toast('Preencha todos os campos.', 4000);
                submutActive(true);
                return false;
            }
            var locationValue =  input.val();
            $.ajax({
                //http://api.openweathermap.org/data/2.5/forecast?q=London
                url: "http://api.openweathermap.org/data/2.5/forecast",
                data: { q: locationValue, appid: "3fed727e458bcfd29fd2a815777eaca2" }
            })
                .done(function( data ) {
                    if (data.cod == 200) {
                        //sometimes data.city.name comes empty string
                        if (data.city.name == "") {
                            window.localStorage.setItem('location', locationValue);
                        } else {
                            window.localStorage.setItem('location', data.city.name);
                        }
                        window.localStorage.setItem('lat', data.city.coord.lat);
                        window.localStorage.setItem('lon', data.city.coord.lon);
                        window.localStorage.setItem('id', data.city.id);
                        window.localStorage.setItem('unit', $("select").val());
                        toast('Salvo com sucesso.', 4000);
                        reloadConfig();
                    } else {
                        toast('Erro: Não foi possível carregar sua localização. Por favor, digite outro local', 4000);
                    }
                })
                .fail(function() {
                    toast('Erro: Não foi possível carregar sua localização. Por favor, tente novamente.', 4000);
                })
                .always(function() {
                    submutActive(true);
                });
            return false;
        });

    }); // end of document ready
})(jQuery); // end of jQuery name space


if (window.localStorage.getItem('location') == null)
    google.maps.event.addDomListener(window, 'load', initializeGeo);
if (window.localStorage.getItem('unit') == null)
    window.localStorage.setItem('unit', "metric");

//fix footer at bottom when content is litle
function fixFooter() {
    if ($("html").height() < $(window).height()) {
        $("footer").css("width", "100%")
            .css("position", "absolute")
            .css("bottom", "0px");
    } else {
        $("footer").css("width", "100%")
            .css("position", "")
            .css("bottom", "");
    }
}

function initializeGeo() {
    // Try HTML5 geolocation
    submutActive(false);
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            $.ajax({
                //http://api.openweathermap.org/data/2.5/forecast?lat=-3.7386675&lon=-38.521766899999996
                url: "http://api.openweathermap.org/data/2.5/forecast",
                data: { lat: position.coords.latitude, lon: position.coords.longitude, appid: "3fed727e458bcfd29fd2a815777eaca2" }
            })
                .done(function( data ) {
                    if (data.cod == 200) {
                        window.localStorage.setItem('location', data.city.name);
                        window.localStorage.setItem('lat', data.city.coord.lat);
                        window.localStorage.setItem('lon', data.city.coord.lon);
                        window.localStorage.setItem('id', data.city.id);
                        if (typeof(loadForecasts) == "function") {
                            loadForecasts();
                        }
                        toast('Localização salva com sucesso.', 4000)
                    } else {
                        window.localStorage.setItem('location', "Disneyland");
                        window.localStorage.setItem('lat', 33.8115);
                        window.localStorage.setItem('lon', -117.915);
                        window.localStorage.setItem('id', "5323810");
                        toast('Erro: Não foi possível carregar sua localização. Por favor, altere nas Configurações.', 4000);
                    }
                })
                .fail(function() {
                    toast('Erro: Não foi possível carregar sua localização. Por favor, tente novamente.', 4000);
                })
                .always(function() {
                    reloadConfig();
                    $('.button-collapse').sideNav('hide');
                    submutActive(true);
                });
        }, function() {
            handleNoGeolocation(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
}

function handleNoGeolocation(errorFlag) {
    var loadElement = $('#load');
    loadElement.empty();
    var info = '<i class="mdi-content-report large"></i>';
    if (errorFlag) {
        info += '<h4>Erro: Não foi possível carregar sua localização.</h4>';
        info += '<h5>Altere sua localização nas Configurações ou compartilhe sua localização. ';
        info += '<a href="config.html">';
        info += '<i class="mdi-action-settings small"></i> Clique aqui!</a>';
        info += '</h5>';
        loadElement.append($(info));
        toast('Erro: Não foi possível carregar sua localização. \n Altere sua localização nas Configurações ou compartilhe sua localização.', 4000);
    } else {
        info += '<h4>Erro: Seu navegador não suporta geolocalização.</h4>';
        info += '<h5>Altere sua localização nas Configurações. ';
        info += '<a href="config.html">';
        info += '<i class="mdi-action-settings small"></i> Clique aqui!</a>';
        info += '</h5>';
        loadElement.append($(info));
        toast('Erro: Seu navegador não suporta geolocalização. Altere sua localização nas Configurações.', 4000);
    }
    submutActive(true);
}

function reloadConfig() {
    $('input[name="location"]').val(window.localStorage.getItem('location'));
    $("select").val(window.localStorage.getItem('unit'));
}

function submutActive(flag) {
    if (flag) {
        $('.mdi-maps-my-location').parent().removeClass("disabled");
        $('button[type="submit"]').removeClass("disabled");
        $('.preloader-wrapper.very-small').removeClass("active");
    } else {
        $('.mdi-maps-my-location').parent().addClass("disabled");
        $('button[type="submit"]').addClass("disabled");
        $('.preloader-wrapper.very-small').addClass("active");
    }
}